package server;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import common.IService;
import common.Service;

public class Server {
	  public Server() {}

	        
	    public static void main(String args[]) {
	        
	        try {
	            IService stub=new Service();
	            
	            // Bind the remote object's stub in the registry
	            Registry registry = LocateRegistry.createRegistry(1200);
	            registry.bind("Service", stub);

	            System.err.println("Server ready");
	        } catch (Exception e) {
	            System.err.println("Server exception: " + e.toString());
	            e.printStackTrace();
	        }
	    }



}
