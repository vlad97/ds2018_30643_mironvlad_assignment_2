package view;

import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.rmi.RemoteException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import common.Car;
import common.IService;

public class ServiceView {
	   private JFrame mainFrame;
	   private IService stub;

	   public ServiceView(IService stub){
	      prepareGUI();
	      this.stub=stub;
	   }
	   private void prepareGUI(){
	      mainFrame = new JFrame("Car Service");
	      mainFrame.setSize(400,400);
	      mainFrame.setLayout(new GridLayout(4, 2));
	      
	      mainFrame.addWindowListener(new WindowAdapter() {
	         public void windowClosing(WindowEvent windowEvent){
	            System.exit(0);
	         }        
	      });    
	      mainFrame.setVisible(true);  
	   }
	   public void showTextFieldDemo(){
	      JLabel  yearlabel= new JLabel("Year: ", JLabel.CENTER);
	      JLabel  engineLabel = new JLabel("Engine Capacity: ", JLabel.CENTER);
	      JLabel  priceLabel = new JLabel("Price: ", JLabel.CENTER);
	      final JTextField yearText = new JTextField(6);
	      final JTextField engineText = new JTextField(6);     
	      final JTextField priceText = new JTextField(6); 

	      JButton computeTaxButton = new JButton("Compute Tax");
	      computeTaxButton.addActionListener(new ActionListener() {
	         public void actionPerformed(ActionEvent e) {     
	     int year=Integer.parseInt(yearText.getText());     
	     int engine=Integer.parseInt(engineText.getText()); 
	     double price=Double.parseDouble(priceText.getText()); 
	     try {
			JOptionPane.showMessageDialog(mainFrame, "Tax: "+stub.calculateTax(new Car(year, engine,price)) );
		} catch (HeadlessException | RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	         }
	      }); 
	      
	      JButton computePriceButton = new JButton("Compute Price");
	      computePriceButton.addActionListener(new ActionListener() {
	         public void actionPerformed(ActionEvent e) {     
	     int year=Integer.parseInt(yearText.getText());     
	     int engine=Integer.parseInt(engineText.getText()); 
	     double price=Double.parseDouble(priceText.getText()); 
	     try {
			JOptionPane.showMessageDialog(mainFrame, "Price: "+stub.calculatePrice(new Car(year, engine,price)) );
		} catch (HeadlessException | RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	         }
	      }); 
	      mainFrame.add(yearlabel);
	      mainFrame.add(yearText);
	      mainFrame.add(engineLabel);
	      mainFrame.add(engineText);
	      mainFrame.add(priceLabel);      
	      mainFrame.add(priceText);
	      mainFrame.add(computeTaxButton);
	      mainFrame.add(computePriceButton);
	      mainFrame.setVisible(true);  
	   }
}
