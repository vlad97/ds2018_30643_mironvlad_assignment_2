package client;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import common.IService;
import view.ServiceView;

public class Client {

    private Client() {}

    public static void main(String[] args) {
    	

        try {
            Registry registry = LocateRegistry.getRegistry(1200);
            IService stub = (IService) registry.lookup("Service");
            ServiceView  serviceView = new ServiceView(stub);      
            serviceView.showTextFieldDemo();
            

        } catch (Exception e) {
            System.err.println("Client exception: " + e.toString());
            e.printStackTrace();
        }
    }
}
