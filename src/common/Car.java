package common;
import java.io.Serializable;

public class Car implements Serializable {

	private static final long serialVersionUID = 1L;
	private int year;
	private int engineCapacity;
	private double price;

	public Car() {
	}

	public Car(int year, int engineCapacity, double price) {
		this.year = year;
		this.engineCapacity = engineCapacity;
		this.price=price;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getEngineCapacity() {
		return engineCapacity;
	}

	public void setEngineCapacity(int engineCapacity) {
		this.engineCapacity = engineCapacity;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

}
