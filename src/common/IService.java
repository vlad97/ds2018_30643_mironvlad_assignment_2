package common;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IService extends Remote {
       public double calculateTax(Car c) throws RemoteException;
       public double calculatePrice(Car c) throws RemoteException;
}
